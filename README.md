[![](https://framagit.org/assets/favicon-075eba76312e8421991a0c1f89a89ee81678bcde72319dd3e8047e2a47cd3a42.ico)](https://framagit.org)

![English:](https://upload.wikimedia.org/wikipedia/commons/thumb/a/ae/Flag_of_the_United_Kingdom.svg/20px-Flag_of_the_United_Kingdom.svg.png) **Framasoft uses GitLab** for the development of its free softwares. Our Github repositories are only mirrors.
If you want to work with us, **fork us on [framagit.org](https://framagit.org)**. (no registration needed, you can sign in with your Github account)

![Français :](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Flag_of_France.svg/20px-Flag_of_France.svg.png) **Framasoft utilise GitLab** pour le développement de ses logiciels libres. Nos dépôts Github ne sont que des miroirs.
Si vous souhaitez travailler avec nous, **forkez-nous sur [framagit.org](https://framagit.org)**. (l'inscription n'est pas nécessaire, vous pouvez vous connecter avec votre compte Github)
* * *

# EP\_HEADREPAIR

At [Framasoft](https://framasoft.org), we host a lot of heavily-used [Etherpad](http://etherpad.org) instances.

The databases are heavily loaded and we often have issues with pads which have a bad head revision: the `head` field in the pad database's record correspond to no revision database's record.

This plugin verifies that the `head` corresponds to an existing revision record.
If it doesn't, the plugin search the more recent existing revision record and modify the pad's head value.

# License

Apache License, version 2. Check the [LICENSE file](LICENSE).