exports.padLoad = function(hook_name, context, cb) {
    headrepair(context.pad, true, function(err) {
        if (err) {
            cb(err);
        }
        cb();
    });
};

function headrepair(pad, firstRound, callback) {
    var head = pad.head;
    if (head > 0) {
        console.info("Check if pad "+pad.id+" need padrepair (head: "+pad.head+")");
        pad.getRevisionChangeset(head, function(err, value) {
            if (value === null) {
                console.info("Bad head: "+head+" for pad "+pad.id);
                if (head > 1) {
                    pad['head'] = head - 1;
                    headrepair(pad, false, callback);
                } else {
                    console.info("Unable to find a good revision for pad "+pad.id);
                    callback("Unable to find a good revision for pad "+pad.id);
                }
            } else {
                if (firstRound === false) {
                    console.info("Found good head revision "+head+" for pad "+pad.id+". Saving to database");
                    pad.saveToDatabase();
                }
                callback();
            }
        });
    } else {
        console.info("Pad "+pad.id+" has head 0. Can't do anything, so don't checking if need head repairing");
        callback();
    }
}

